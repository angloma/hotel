
<%@page import="modelo.Persona"%>
<%@page import="java.util.ArrayList"%>


<%@page import="java.util.List"%>
<%@page import="modelo.Habitacion"%>
<%@page import="modelo.Reservacion"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE HTML>

<html>
<head>
<title>Angloma Resorts a Hotel and Restaurants Category Flat Bootstarp Resposive Website Template | Home :: w3layouts</title>
<link href="resource/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<link href="resource/css/style.css" rel="stylesheet" type="text/css" media="all" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Luxury Resorts  Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="resource/css/flexslider.css" type="text/css" media="screen" />
<script src="resource/js/jquery-1.8.3.min.js"></script>
<script src="resource/js/responsiveslides.min.js"></script>
	 <script>
		$(function () {
		  $("#slider1").responsiveSlides({
			auto: true,
			speed: 500,
			namespace: "callbacks",
			pager: true,
		  });
		});
	  </script>
	<!----->
<!---- start-smoth-scrolling---->
<script type="text/javascript" src="resource/js/move-top.js"></script>
<script type="text/javascript" src="resource/js/easing.js"></script>
 <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
</script>
	<script type="text/javascript">
		$(document).ready(function() {
				/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
				*/
		$().UItoTop({ easingType: 'easeOutQuart' });
});
</script>
<!---End-smoth-scrolling---->
<link rel="stylesheet" href="resource/css/swipebox.css">
			<script src="resource/js/jquery.swipebox.min.js"></script> 
			    <script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
</script>
</head>
<body>
	<div class="header" id="home">
		<div class="header-top">
				<div class="container">
			<div class="logo">
				<a href="index.jsp">Angloma <span>resorts</span></a>
				</div>
				<div class="top-menu">
					<span class="menu"><img src="resource/images/nav.png" alt=""/> </span>
                      <ul>
						<nav class="cl-effect-3">
					<li><a href="#home" class="active scroll">home</a></li>
						<li><a href="#aboutus" class="scroll">Acerca de Nosotros </a></li>
						 	<li><a href="#services" class="scroll">Registrarse</a></li>
								 <li><a href="#gallery" class="scroll">Galeria</a></li>
								 	<li><a href="#reviews" class="scroll">Opciones</a></li>
									 		<li><a href="#pricing" class="scroll">Reservacion</a></li>
												<li><a href="#blog" class="scroll">blog</a></li>
							   						 <li><a href="#contact" class="scroll">Contacto</a></li>
						                             </nav>
						 </ul>
					</div>
					 <!--script-nav-->
		 <script>
		 $("span.menu").click(function(){
		 $(".top-menu ul").slideToggle("slow" , function(){
		 });
		 });
		 </script>
			<div class="clearfix"></div>
			</div>
	</div>
				<section class="slider">
						<div class="flexslider">
							<ul class="slides">
								<li>
									<div class="slider-info">
										<img src="resource/images/banner.jpg" class="img-responsive" alt="">
										
									</div>
								</li>
								<li>
									<div class="slider-info">
										<img src="resource/images/banner1.jpg" class="img-responsive" alt="">
										
									</div>
								</li>
								<li>	
									<div class="slider-info">
										<img src="resource/images/banner2.jpg" class="img-responsive" alt="">
										
									</div>
								</li>
								<li>	
									<div class="slider-info">
										<img src="resource/images/banner3.jpg" class="img-responsive" alt="">
										
									</div>
								</li>
								<li>	
									<div class="slider-info">
										<img src="resource/images/banner4.jpg" class="img-responsive" alt="">
										
									</div>
								</li>
							</ul>
						</div>
					</section>
						<!-- FlexSlider -->
							  <script defer src="resource/js/jquery.flexslider.js"></script>
							  <script type="text/javascript">
								$(function(){
								  SyntaxHighlighter.all();
								});
								$(window).load(function(){
								  $('.flexslider').flexslider({
									animation: "slide",
									start: function(slider){
									  $('body').removeClass('loading');
									}
								  });
								});
							  </script>
						<!-- FlexSlider -->
	<!-- slider -->
						</div>
						<div class="content">
							<div class="about-section" id="aboutus">
								<div class="container">
									<h3>Acerca de <span>Nosotros<span></h3>
									<div class="about-grids">
										<div class="col-md-3 about-grid">
											<img src="resource/images/page1_img1.jpg">
											<h4>Bienestar y<span>Spa</span></h4>
											<p>Masajes de Relajación,Terapia de piedras calientes,Masajes descontracturantes,Masaje 4 manos,Reflexología</p>
										</div>
										<div class="col-md-3 about-grid">
											<img src="resource/images/page1_img2.jpg">
											<h4>Espacio y <span>Tarifas</span></h4>
											<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
										</div>
										<div class="col-md-3 about-grid">
											<img src="resource/images/page1_img3.jpg">
											<h4>Spa y <span>Reparacion</span></h4>
											<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
										</div>
										<div class="col-md-3 about-grid">
											<img src="resource/images/page1_img4.jpg">
											<h4>Ambiente y<span>Restaurante</span></h4>
											<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
										</div>
										<div class="clearfix"></div>
									</div>
									<a class="button hvr-shutter-in-vertical" href="#">read more</a>
									</div>
									</div>
									<div class="services-section" id="services">
										<div class="container">
											<h3>Registro <span>Usuario</span></h3>																	
                                                                                        <div class="contact-grid">
								<div class="col-md-6 contactdetails-grid">                                                                    
                                                                    
<form id="addUsuario" name="addUsuario" action="ServletUsuario?a=saveUsuario" method="POST">
    <h5> <label>Documento:</label> </h5>
    	    <input name="documento" type="text"/><br/>                
            <h5><label>Nombre:</label></h5>
            <input name="nombre" type="text"/><br/>
            <h5> <label>Apellido:</label></h5>
            <input name="apellido" type="text"/><br/>
            <h5> <label>Edad:</label></h5>
            <input name="edad" type="text"/><br/>
            <h5> <label>Genero:</label></h5>
            <div class="form-group">
	       <select class="form-control" type="select" name="genero">
		<option>Femenino</option>
		<option>Masculino</option>
		</select>
	    </div>
            <h5><label>Telefono:</label></h5>
            <input name="telefono" type="text"/><br/><br/>
            <h5> <label>Usuario:</label></h5>
            <input name="usuario" type="text"/><br/>
            <h5> <label>Contraseña:</label></h5>
            <input name="contrasena" type="password"/><br/><br/>
             <button type="reset" class="btn btn-default">Limpiar</button>
             <button type="submit" id="sendForm">Registrar</button> 
           
    </form>                  
                                                                </div>	
                                                                    
                                                                    <div class="clearfix"> </div>
							</div>
							</div>
							<div class="clearfix"> </div>
							
								</div>
							</div>
						</div>
						<div class="gallery-section" id="gallery">
								<h3>Gall<span>ery</span></h3>
								<div class="gallery-grids">
							<div class="col-md-2 gallery-grid">
								<div class="gallery-grid1">
									<a href="resource/images/img1.jpg" class="swipebox"><img src="resource/images/img1.jpg"/></a>
							</div>
							<div class="gallery-grid1">
									<a href="resource/images/img2.jpg" class="swipebox"><img src="resource/images/img2.jpg"/></a>
							</div>
							<div class="gallery-grid1">
									<a href="resource/images/img3.jpg" class="swipebox"><img src="resource/images/img3.jpg"/></a>
							</div>
						</div>
						<div class="col-md-5 gallery-grid10">
							<div class="gallery-grid01">
									<a href="resource/images/img5.jpg"class="swipebox"><img src="resource/images/img5.jpg"/></a>
							</div>
							<div class="gallery1-grid">
							<div class="gallery-grid02 ">
									<a href="resource/images/img4.jpg" class="swipebox"><img src="resource/images/img4.jpg"/></a>
							</div>
							<div class="gallery-grid03">
									<a href="resource/images/img7.jpg" class="swipebox"><img src="resource/images/img7.jpg"/></a>
							</div>
							<div class="clearfix"> </div>
							</div>
							</div>
							<div class="col-md-5 gallery-grid11">
							<div class="gallery2-grid">
							<div class="gallery-grid02">
									<a href="resource/images/img8.jpg"class="swipebox"><img src="resource/images/img8.jpg"/></a>
							</div>
							<div class="gallery-grid03">
									<a href="resource/images/img9.jpg" class="swipebox"><img src="resource/images/img9.jpg"/></a>
							</div>
							<div class="clearfix"> </div>
							</div>
							<div class="gallery-grid01">
							<a href="resource/images/img6.jpg"class="swipebox"><img src="resource/images/img6.jpg"/></a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
						</div>
                                               
                                                <div class="services-section" id="services">
										<div class="container">
						<div class="price-section" id="pricing">
							<div class="price-tables">
		<div class="container">
			<h3>Registrar <span>Reservacion</span></h3>
                        
                            <div class="contact-grid">
								<div class="col-md-6 contactdetails-grid">     
                        
			<form id="addreservacion" name="addreservacion" action="ServletReservacion?h=SaveReservacion" method="POST">
                   

                 <h5> <label>Seleccione una Habitacion:</label></h5>   
                <div class="form-group">
 
                 <select name="Habitacion" Id="idhab" method="POST" class="form-control" type="select" >
                     <option selected>Seleccione una habitacion</option> 
                     
                                <%
                                    Habitacion hb = new Habitacion();
                                    ArrayList<Habitacion> arrhabitacion = hb.getHabitacion();
                                            for(Habitacion hbt: arrhabitacion){
                                       out.println("<option value='"+hbt.getIdHabitacion()+"'>"+hbt.getNumeroHabitacion()+"</option>");
                                %>
                                    
                                <%
                                   }
                                %>
                   
                 
  
                 </select> <br><br> </div>
                  <h5> <label>Seleccione una Persona:</label></h5> 
<div class="form-group">
               <select name="Persona" Id="idper" method="POST" class="form-control" type="select">
                    <option selected >Seleccione Una persona</option> 
                     
                                <%
                                    Persona per = new Persona();
                                    ArrayList<Persona> arrtt = per.getPersona();
                                            for(Persona ttr: arrtt){
                                       out.println("<option value='"+ttr.getIdPersona()+"'>"+ttr.getNombre()+"</option>");
                                %>
                                    
                                <%
                                   }
                                %>
                </select><br><br>  </div>
                
            <h5><label>fecha de Reservacion:</label></h5>
            <input name="fechaReservacion" type="date"/><br/>
            <br/>
            <h5> <label>hora de Ingreso:</label></h5>
            <input name="horaIngreso" type="text"/><br/>
            <br/>
            <h5> <label>hora de Salida:</label></h5>
            <input name="horaSalida" type="text"/><br/>
            <br/>
            <h5> <label>costo:</label></h5>
            <input name="costo" type="text"/><br/>
            <br><br>
<button type="reset" class="btn btn-default">Limpiar</button>
<button type="submit" class="button">Registrar</button>

  </form>
		</div>
	</div>
     </div>
                       
                                                                    <div class="clearfix"> </div>
							</div>
							</div>
							<div class="clearfix"> </div>
							
								</div>
							</div>

</div>
	<!----//End-price-tables----->
	<div class="team-section" id="reviews">
		<h3>Revi<span>ews</span></h3>
			<div class="slider1">	  
	  <div class="callbacks_container">
	      <ul class="rslides" id="slider1">
	         <li>
				 <img src="resource/images/side-1.png" alt=""/>
				  <div class="caption caption1">
					<h4>john doe</h4>
					<p>It?s good to have everything sorted. Taasky offers categories which can filter your tasks in a very simple manner. Every category has its own colour to avoid chaos.</p>
				  </div>
	         </li>
	         <li>
				 <img src="resource/images/side-2.png" alt=""/>
				  <div class="caption caption1">
					<h4>Robert Smith</h4>
					<p>It?s good to have everything sorted. Taasky offers categories which can filter your tasks in a very simple manner. Every category has its own colour to avoid chaos.</p>
				  </div>
	         </li>
	         <li>
				 <img src="resource/images/side-3.png" alt=""/>
				  <div class="caption caption1">
					<h4> Steve Smith </h4>
					<p>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutem. Lorem ipsum dolor sit amet, conectetu er adipiscing elit.vestibulum.</p>
				  </div>
	         </li>
	      </ul>	      
      </div>
</div>
</div>
	<div class="blog-section" id="blog">
		<div class="container">
			<h3>bl<span>og<span></h3>
			<div class="blog-grid">
				<div class="col-md-6 blog-leftgtid">
					<div class="blog-grid">
					<a href="single.html"><img src="resource/images/pic1.jpg"/></a>
						<a href="single.html">SED UT PERSPICIATIS UNDE</a>
							<p class="date">02-01-15 by <a href="#">Finibus Bonorum</a></p>
							<p>Praesent vestim molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
							</div>
							<div class="blog-grid1">
			<a href="single.html"><img src="resource/images/pic2.jpg"/></a>
					<a href="single.html">DONEC SAGITTIS EUISMOD PURUS</a>
							<p class="date">09-02-15 by <a href="#">Finibus Bonorum</a></p>
							<p>Praesent vestim molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
							</div>
						</div>
						<div class="col-md-6 blog-rightgtid">
					<div class="blog-grid">
					<a href="single.html"><img src="resource/images/pic3.jpg"/></a>
						<a href="single.html">SED UT PERSPICIATIS UNDE</a>
							<p class="date">19-01-15 by <a href="#">Finibus Bonorum</a></p>
							<p>Praesent vestim molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
							</div>
							<div class="blog-grid1">
		<a href="single.html"><img src="resource/images/pic4.jpg"/></a>
					<a href="single.html">DONEC SAGITTIS EUISMOD PURUS</a>
							<p class="date">29-02-15 by <a href="#">Finibus Bonorum</a></p>
							<p>Praesent vestim molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
							</div>
						</div>
						<div class="clearfix"> </div>
						</div>
						</div>
						</div>
						<div class="contact-section" id="contact">
							<div class="container">
								<h3>contact <span>us</span></h3>
								<div class="contact-grid">
								<div class="col-md-6 contactdetails-grid">
						<h4>contact form</h4>
               				<h5>name <span>*</span></h5>
               					<input type="text">
               						<h5>email address <span>*</span></h5>
               							<input type="text">
               								<h5>subject <span>*</span></h5>
               									<input type="text">
               										<h5>message <span>*</span></h5>
               											<textarea> </textarea>
             												<input type="button" value="send">
               				</div>
               			<div class="col-md-6 contactdetails-grid1">
               				<div class="address">
               				<h4>Address</h4>
               			<p>500 Lorem Ipsum Dolor Sit,</p>
							<p>22-56-2-9 Sit Amet, Lorem,</p>
								<p>USA</p>
				   					<p>Phone:(00) 222 666 444</p>
				   						<p>Fax: (000) 000 00 00 0</p>
				 	 						<p>Email: <a href="mailto:example@mail.com">info@mycompany.com</a></p>
				   								<p>Follow on: <a href="#">Facebook</a> , <a href="#">Twitter</a></p>
				  							</div>
				  							<div class="google-map">
               				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3021.0814617966994!2d-73.96467908332265!3d40.782223218920294!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2589a018531e3%3A0xb9df1f7387a94119!2sCentral+Park!5e0!3m2!1sen!2sin!4v1420805667126"></iframe>
               			</div>
				   						</div>
										<div class="clearfix"> </div>
               					</div>
								</div>
							</div>
					</div>
					<div class="footer-section">
						<div class="container">
							<div class="footer-top">
								<div class="social-icons">
										<a href="#"><i class="icon1"></i></a>
										<a href="#"><i class="icon2"></i></a>
										<a href="#"><i class="icon3"></i></a>
									</div>
								</div>
								<div class="footer-bottom">
									<p> Copyright &copy;2015  All rights  Reserved | Design by<a href="http://w3layouts.com" target="target_blank">W3Layouts</a></p>
									</div>
					<script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
					</div>
			</div>
</body>
</html>