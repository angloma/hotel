package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class VerHabitacion_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Angloma Resorts a Hotel and Restaurants Category Flat Bootstarp Resposive Website Template | Home :: w3layouts</title>\n");
      out.write("<link href=\"resource/css/bootstrap.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\">\n");
      out.write("<link href=\"resource/css/style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("<meta name=\"keywords\" content=\"Luxury Resorts  Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, \n");
      out.write("Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design\" />\n");
      out.write("<script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900' rel='stylesheet' type='text/css'>\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic' rel='stylesheet' type='text/css'>\n");
      out.write("<link rel=\"stylesheet\" href=\"resource/css/flexslider.css\" type=\"text/css\" media=\"screen\" />\n");
      out.write("<script src=\"resource/js/jquery-1.8.3.min.js\"></script>\n");
      out.write("<script src=\"resource/js/responsiveslides.min.js\"></script>\n");
      out.write("\t <script>\n");
      out.write("\t\t$(function () {\n");
      out.write("\t\t  $(\"#slider1\").responsiveSlides({\n");
      out.write("\t\t\tauto: true,\n");
      out.write("\t\t\tspeed: 500,\n");
      out.write("\t\t\tnamespace: \"callbacks\",\n");
      out.write("\t\t\tpager: true,\n");
      out.write("\t\t  });\n");
      out.write("\t\t});\n");
      out.write("\t  </script>\n");
      out.write("\t<!----->\n");
      out.write("<!---- start-smoth-scrolling---->\n");
      out.write("<script type=\"text/javascript\" src=\"resource/js/move-top.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resource/js/easing.js\"></script>\n");
      out.write(" <script type=\"text/javascript\">\n");
      out.write("\t\tjQuery(document).ready(function($) {\n");
      out.write("\t\t\t$(\".scroll\").click(function(event){\t\t\n");
      out.write("\t\t\t\tevent.preventDefault();\n");
      out.write("\t\t\t\t$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);\n");
      out.write("\t\t\t});\n");
      out.write("\t\t});\n");
      out.write("</script>\n");
      out.write("\t<script type=\"text/javascript\">\n");
      out.write("\t\t$(document).ready(function() {\n");
      out.write("\t\t\t\t/*\n");
      out.write("\t\t\t\tvar defaults = {\n");
      out.write("\t\t\t\tcontainerID: 'toTop', // fading element id\n");
      out.write("\t\t\t\tcontainerHoverID: 'toTopHover', // fading element hover id\n");
      out.write("\t\t\t\tscrollSpeed: 1200,\n");
      out.write("\t\t\t\teasingType: 'linear' \n");
      out.write("\t\t\t\t};\n");
      out.write("\t\t\t\t*/\n");
      out.write("\t\t$().UItoTop({ easingType: 'easeOutQuart' });\n");
      out.write("});\n");
      out.write("</script>\n");
      out.write("<!---End-smoth-scrolling---->\n");
      out.write("<link rel=\"stylesheet\" href=\"resource/css/swipebox.css\">\n");
      out.write("\t\t\t<script src=\"resource/js/jquery.swipebox.min.js\"></script> \n");
      out.write("\t\t\t    <script type=\"text/javascript\">\n");
      out.write("\t\t\t\t\tjQuery(function($) {\n");
      out.write("\t\t\t\t\t\t$(\".swipebox\").swipebox();\n");
      out.write("\t\t\t\t\t});\n");
      out.write("</script>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\t<div class=\"header\" id=\"home\">\n");
      out.write("\t\t<div class=\"header-top\">\n");
      out.write("\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t<div class=\"logo\">\n");
      out.write("\t\t\t\t<a href=\"index.jsp\">Angloma <span>resorts</span></a>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"top-menu\">\n");
      out.write("\t\t\t\t\t<span class=\"menu\"><img src=\"resource/images/nav.png\" alt=\"\"/> </span>\n");
      out.write("                      <ul>\n");
      out.write("\t\t\t\t\t\t<nav class=\"cl-effect-3\">\n");
      out.write("\t\t\t\t\t<li><a href=\"#home\" class=\"active scroll\">home</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#aboutus\" class=\"scroll\">Ver Usuarios </a></li>\n");
      out.write("\t\t\t\t\t\t \t<li><a href=\"#services\" class=\"scroll\">Ver Empleados</a></li>\n");
      out.write("                                                        <li><a href=\"#pricing\" class=\"scroll\">Ver Habitaciones</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#blog\" class=\"scroll\">Ver Asignaciones</a></li>\n");
      out.write("                                                        \n");
      out.write("\t\t\t\t\t\t\t\t <li><a href=\"#gallery\" class=\"scroll\">Galeria</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t \t<li><a href=\"#reviews\" class=\"scroll\">Opciones</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t\t \t\t\n");
      out.write("\t\t\t\t\t\t\t   \t\t\t\t\t\t <li><a href=\"#contact\" class=\"scroll\">Contacto</a></li>\n");
      out.write("\t\t\t\t\t\t                             </nav>\n");
      out.write("\t\t\t\t\t\t </ul>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t <!--script-nav-->\n");
      out.write("\t\t <script>\n");
      out.write("\t\t $(\"span.menu\").click(function(){\n");
      out.write("\t\t $(\".top-menu ul\").slideToggle(\"slow\" , function(){\n");
      out.write("\t\t });\n");
      out.write("\t\t });\n");
      out.write("\t\t </script>\n");
      out.write("\t\t\t<div class=\"clearfix\"></div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("\t\t\t\t<section class=\"slider\">\n");
      out.write("\t\t\t\t\t\t<div class=\"flexslider\">\n");
      out.write("\t\t\t\t\t\t\t<ul class=\"slides\">\n");
      out.write("\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"slider-info\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<img src=\"resource/images/banner.jpg\" class=\"img-responsive\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"slider-info\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<img src=\"resource/images/banner1.jpg\" class=\"img-responsive\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t<li>\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"slider-info\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<img src=\"resource/images/banner2.jpg\" class=\"img-responsive\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t<li>\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"slider-info\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<img src=\"resource/images/banner3.jpg\" class=\"img-responsive\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t<li>\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"slider-info\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<img src=\"resource/images/banner4.jpg\" class=\"img-responsive\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</section>\n");
      out.write("\t\t\t\t\t\t<!-- FlexSlider -->\n");
      out.write("\t\t\t\t\t\t\t  <script defer src=\"resource/js/jquery.flexslider.js\"></script>\n");
      out.write("\t\t\t\t\t\t\t  <script type=\"text/javascript\">\n");
      out.write("\t\t\t\t\t\t\t\t$(function(){\n");
      out.write("\t\t\t\t\t\t\t\t  SyntaxHighlighter.all();\n");
      out.write("\t\t\t\t\t\t\t\t});\n");
      out.write("\t\t\t\t\t\t\t\t$(window).load(function(){\n");
      out.write("\t\t\t\t\t\t\t\t  $('.flexslider').flexslider({\n");
      out.write("\t\t\t\t\t\t\t\t\tanimation: \"slide\",\n");
      out.write("\t\t\t\t\t\t\t\t\tstart: function(slider){\n");
      out.write("\t\t\t\t\t\t\t\t\t  $('body').removeClass('loading');\n");
      out.write("\t\t\t\t\t\t\t\t\t}\n");
      out.write("\t\t\t\t\t\t\t\t  });\n");
      out.write("\t\t\t\t\t\t\t\t});\n");
      out.write("\t\t\t\t\t\t\t  </script>\n");
      out.write("\t\t\t\t\t\t<!-- FlexSlider -->\n");
      out.write("\t<!-- slider -->\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"content\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"about-section\" id=\"aboutus\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<h3>Acerca de <span>Nosotros<span></h3>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"about-grids\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 about-grid\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<img src=\"resource/images/page1_img1.jpg\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<h4>Bienestar y<span>Spa</span></h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<p>Masajes de Relajación,Terapia de piedras calientes,Masajes descontracturantes,Masaje 4 manos,Reflexología</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 about-grid\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<img src=\"resource/images/page1_img2.jpg\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<h4>Espacio y <span>Tarifas</span></h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 about-grid\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<img src=\"resource/images/page1_img3.jpg\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<h4>Spa y <span>Reparacion</span></h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 about-grid\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<img src=\"resource/images/page1_img4.jpg\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<h4>Ambiente y<span>Restaurante</span></h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t<a class=\"button hvr-shutter-in-vertical\" href=\"#\">read more</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"services-section\" id=\"services\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<h3>Registro <span>Usuario</span></h3>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n");
      out.write("                                                                                        \n");
      out.write("                                                                                        \n");
      out.write("\t\t\t\t\t\t<div class=\"gallery-section\" id=\"gallery\">\n");
      out.write("\t\t\t\t\t\t\t\t<h3>Gall<span>ery</span></h3>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"gallery-grids\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-md-2 gallery-grid\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"gallery-grid1\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"resource/images/img1.jpg\" class=\"swipebox\"><img src=\"resource/images/img1.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery-grid1\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"resource/images/img2.jpg\" class=\"swipebox\"><img src=\"resource/images/img2.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery-grid1\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"resource/images/img3.jpg\" class=\"swipebox\"><img src=\"resource/images/img3.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"col-md-5 gallery-grid10\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery-grid01\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"resource/images/img5.jpg\"class=\"swipebox\"><img src=\"resource/images/img5.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery1-grid\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery-grid02 \">\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"resource/images/img4.jpg\" class=\"swipebox\"><img src=\"resource/images/img4.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery-grid03\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"resource/images/img7.jpg\" class=\"swipebox\"><img src=\"resource/images/img7.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-md-5 gallery-grid11\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery2-grid\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery-grid02\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"resource/images/img8.jpg\"class=\"swipebox\"><img src=\"resource/images/img8.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery-grid03\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"resource/images/img9.jpg\" class=\"swipebox\"><img src=\"resource/images/img9.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"gallery-grid01\">\n");
      out.write("\t\t\t\t\t\t\t<a href=\"resource/images/img6.jpg\" class=\"swipebox\"><img src=\"resource/images/img6.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"price-section\" id=\"pricing\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"price-tables\">\n");
      out.write("\t\t<div class=\"container\">\n");
      out.write("\t\t\t<h3>Habita<span>Ciones</span></h3>\n");
      out.write("\t\t\t<div class=\"price-table-grids\">\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t<h1>Consultar Listado de Canciones</h1>\n");
      out.write("                                <form>\n");
      out.write("        <table>\n");
      out.write("        \n");
      out.write("        ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        </table>\n");
      out.write("            \n");
      out.write("\t\t\t\t<div class=\"clear\"> </div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("\t\n");
      out.write("\t</div>\n");
      out.write("\t<!----//End-price-tables----->\n");
      out.write("\t<div class=\"team-section\" id=\"reviews\">\n");
      out.write("\t\t<h3>Revi<span>ews</span></h3>\n");
      out.write("\t\t\t<div class=\"slider1\">\t  \n");
      out.write("\t  <div class=\"callbacks_container\">\n");
      out.write("\t      <ul class=\"rslides\" id=\"slider1\">\n");
      out.write("\t         <li>\n");
      out.write("\t\t\t\t <img src=\"resource/images/side-1.png\" alt=\"\"/>\n");
      out.write("\t\t\t\t  <div class=\"caption caption1\">\n");
      out.write("\t\t\t\t\t<h4>john doe</h4>\n");
      out.write("\t\t\t\t\t<p>It?s good to have everything sorted. Taasky offers categories which can filter your tasks in a very simple manner. Every category has its own colour to avoid chaos.</p>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t         </li>\n");
      out.write("\t         <li>\n");
      out.write("\t\t\t\t <img src=\"resource/images/side-2.png\" alt=\"\"/>\n");
      out.write("\t\t\t\t  <div class=\"caption caption1\">\n");
      out.write("\t\t\t\t\t<h4>Robert Smith</h4>\n");
      out.write("\t\t\t\t\t<p>It?s good to have everything sorted. Taasky offers categories which can filter your tasks in a very simple manner. Every category has its own colour to avoid chaos.</p>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t         </li>\n");
      out.write("\t         <li>\n");
      out.write("\t\t\t\t <img src=\"resource/images/side-3.png\" alt=\"\"/>\n");
      out.write("\t\t\t\t  <div class=\"caption caption1\">\n");
      out.write("\t\t\t\t\t<h4> Steve Smith </h4>\n");
      out.write("\t\t\t\t\t<p>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutem. Lorem ipsum dolor sit amet, conectetu er adipiscing elit.vestibulum.</p>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t         </li>\n");
      out.write("\t      </ul>\t      \n");
      out.write("      </div>\n");
      out.write("</div>\n");
      out.write("</div>\n");
      out.write("\t<div class=\"blog-section\" id=\"blog\">\n");
      out.write("\t\t<div class=\"container\">\n");
      out.write("\t\t\t<h3>bl<span>og<span></h3>\n");
      out.write("\t\t\t<div class=\"blog-grid\">\n");
      out.write("\t\t\t\t<div class=\"col-md-6 blog-leftgtid\">\n");
      out.write("\t\t\t\t\t<div class=\"blog-grid\">\n");
      out.write("\t\t\t\t\t<a href=\"single.html\"><img src=\"resource/images/pic1.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t<a href=\"single.html\">SED UT PERSPICIATIS UNDE</a>\n");
      out.write("\t\t\t\t\t\t\t<p class=\"date\">02-01-15 by <a href=\"#\">Finibus Bonorum</a></p>\n");
      out.write("\t\t\t\t\t\t\t<p>Praesent vestim molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"blog-grid1\">\n");
      out.write("\t\t\t<a href=\"single.html\"><img src=\"resource/images/pic2.jpg\"/></a>\n");
      out.write("\t\t\t\t\t<a href=\"single.html\">DONEC SAGITTIS EUISMOD PURUS</a>\n");
      out.write("\t\t\t\t\t\t\t<p class=\"date\">09-02-15 by <a href=\"#\">Finibus Bonorum</a></p>\n");
      out.write("\t\t\t\t\t\t\t<p>Praesent vestim molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"col-md-6 blog-rightgtid\">\n");
      out.write("\t\t\t\t\t<div class=\"blog-grid\">\n");
      out.write("\t\t\t\t\t<a href=\"single.html\"><img src=\"resource/images/pic3.jpg\"/></a>\n");
      out.write("\t\t\t\t\t\t<a href=\"single.html\">SED UT PERSPICIATIS UNDE</a>\n");
      out.write("\t\t\t\t\t\t\t<p class=\"date\">19-01-15 by <a href=\"#\">Finibus Bonorum</a></p>\n");
      out.write("\t\t\t\t\t\t\t<p>Praesent vestim molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"blog-grid1\">\n");
      out.write("\t\t<a href=\"single.html\"><img src=\"resource/images/pic4.jpg\"/></a>\n");
      out.write("\t\t\t\t\t<a href=\"single.html\">DONEC SAGITTIS EUISMOD PURUS</a>\n");
      out.write("\t\t\t\t\t\t\t<p class=\"date\">29-02-15 by <a href=\"#\">Finibus Bonorum</a></p>\n");
      out.write("\t\t\t\t\t\t\t<p>Praesent vestim molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"clearfix\"> </div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"contact-section\" id=\"contact\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"container\">\n");
      out.write("                                                            <h3>Ver Asignaciones</h3>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"contact-grid\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"col-md-6 contactdetails-grid\">\n");
      out.write("                                                                    <div>\n");
      out.write("                                                                        <form name=\"asignacion\" action=\"ServletsAsignacion?a=viewAsignaciones\" method=\"POST\">\n");
      out.write("                                                                            <table border=\"1px\">\n");
      out.write("                                                                                <thead>\n");
      out.write("                                                                                    <tr>\n");
      out.write("                                                                                        <td>Empleado</td>\n");
      out.write("                                                                                        <td>Habitacion</td>\n");
      out.write("                                                                                        <td>Tipo Trabajo</td>\n");
      out.write("                                                                                    </tr>\n");
      out.write("                                                                                   \n");
      out.write("                                                                                </thead>\n");
      out.write("                                                                                ");
      if (_jspx_meth_c_forEach_1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                                                               \n");
      out.write("                                                                            </table> \n");
      out.write("                                                                        </form>\n");
      out.write("                                                                    </div>\n");
      out.write("                                                                                \n");
      out.write("           \n");
      out.write("                \n");
      out.write("               \t\t\t<div class=\"col-md-6 contactdetails-grid1\">\n");
      out.write("               \t\t\t\t<div class=\"address\">\n");
      out.write("               \t\t\t\t<h4>Address</h4>\n");
      out.write("               \t\t\t<p>500 Lorem Ipsum Dolor Sit,</p>\n");
      out.write("\t\t\t\t\t\t\t<p>22-56-2-9 Sit Amet, Lorem,</p>\n");
      out.write("\t\t\t\t\t\t\t\t<p>USA</p>\n");
      out.write("\t\t\t\t   \t\t\t\t\t<p>Phone:(00) 222 666 444</p>\n");
      out.write("\t\t\t\t   \t\t\t\t\t\t<p>Fax: (000) 000 00 00 0</p>\n");
      out.write("\t\t\t\t \t \t\t\t\t\t\t<p>Email: <a href=\"mailto:example@mail.com\">info@mycompany.com</a></p>\n");
      out.write("\t\t\t\t   \t\t\t\t\t\t\t\t<p>Follow on: <a href=\"#\">Facebook</a> , <a href=\"#\">Twitter</a></p>\n");
      out.write("\t\t\t\t  \t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t  \t\t\t\t\t\t\t<div class=\"google-map\">\n");
      out.write("               \t\t\t\t<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3021.0814617966994!2d-73.96467908332265!3d40.782223218920294!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2589a018531e3%3A0xb9df1f7387a94119!2sCentral+Park!5e0!3m2!1sen!2sin!4v1420805667126\"></iframe>\n");
      out.write("               \t\t\t</div>\n");
      out.write("\t\t\t\t   \t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>\n");
      out.write("               \t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"footer-section\">\n");
      out.write("\t\t\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"footer-top\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"social-icons\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"icon1\"></i></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"icon2\"></i></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"icon3\"></i></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"footer-bottom\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<p> Copyright &copy;2015  All rights  Reserved | Design by<a href=\"http://w3layouts.com\" target=\"target_blank\">W3Layouts</a></p>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<script type=\"text/javascript\">\n");
      out.write("\t\t\t\t\t\t$(document).ready(function() {\n");
      out.write("\t\t\t\t\t\t\t/*\n");
      out.write("\t\t\t\t\t\t\tvar defaults = {\n");
      out.write("\t\t\t\t\t  \t\t\tcontainerID: 'toTop', // fading element id\n");
      out.write("\t\t\t\t\t\t\t\tcontainerHoverID: 'toTopHover', // fading element hover id\n");
      out.write("\t\t\t\t\t\t\t\tscrollSpeed: 1200,\n");
      out.write("\t\t\t\t\t\t\t\teasingType: 'linear' \n");
      out.write("\t\t\t\t\t \t\t};\n");
      out.write("\t\t\t\t\t\t\t*/\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t$().UItoTop({ easingType: 'easeOutQuart' });\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t});\n");
      out.write("\t\t\t\t\t</script>\n");
      out.write("\t\t\t\t<a href=\"#\" id=\"toTop\" style=\"display: block;\"> <span id=\"toTopHover\" style=\"opacity: 1;\"> </span></a>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${canciones}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("can");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("            <tr><td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${can.nombre}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("           <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${can.artista}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("            <td><a href=\"Cancion?c=update&i=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${can.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">Editar</a></td>\n");
          out.write("            <td><a href=\"Cancion?c=delete&i=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${can.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">Eliminar</a></td>\n");
          out.write("            \n");
          out.write("            </tr>\n");
          out.write("            \n");
          out.write("            \n");
          out.write("        ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_forEach_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_1.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_1.setParent(null);
    _jspx_th_c_forEach_1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${asignacion}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_1.setVar("asg");
    int[] _jspx_push_body_count_c_forEach_1 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_1 = _jspx_th_c_forEach_1.doStartTag();
      if (_jspx_eval_c_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                                                                     <tr>\n");
          out.write("                                                                                    <td>{asg.Empleado}</td>  \n");
          out.write("                                                                                    <td>{asg.Habitacion}</td>  \n");
          out.write("                                                                                    <td>{asg.Tipotrabajo}</td>  \n");
          out.write("                                                                                </tr>\n");
          out.write("                                                                                ");
          int evalDoAfterBody = _jspx_th_c_forEach_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_1.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_1);
    }
    return false;
  }
}
