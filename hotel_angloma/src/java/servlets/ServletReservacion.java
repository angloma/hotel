/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Persona;
import modelo.Habitacion;
import modelo.Reservacion;
import modelo.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author mhafe
 */
@WebServlet(name = "ServletReservacion", urlPatterns = {"/ServletReservacion"})
public class ServletReservacion extends HttpServlet {

    
          Habitacion hb= new Habitacion();
          Persona per = new Persona();
        
           private void saveReservacion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            PrintWriter out = response.getWriter();
            int idPersona=Integer.parseInt(request.getParameter("Persona"));  
            int idHabitacion=Integer.parseInt(request.getParameter("Habitacion"));
             
        
            hb = hb.numhabitacion(idHabitacion);
            per= per.nombre(idPersona);
          
           Reservacion h = new Reservacion(hb,per,request.getParameter("fechaReservacion"), request.getParameter("horaIngreso"),request.getParameter("horaSalida"),Integer.parseInt(request.getParameter("costo")));
            
            Session s = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = s.beginTransaction();
            s.save(h);
            //Obtenemos la session del cliente
            HttpSession sh = request.getSession();
            sh.setAttribute("Reservacion", h);
            tx.commit();
            s.close();
            response.sendRedirect("index.jsp");
        
       
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        if(request.getParameter("h").equalsIgnoreCase("saveReservacion")){
            saveReservacion(request,response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
